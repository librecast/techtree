# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
# Copyright (c) 2023 Brett Sheffield <bacs@librecast.net>

CFLAGS = -g -Og -Wall -Wextra -pedantic
PROGRAM = techtree
INSTALL = cp
INSTALL_DIR = /usr/local/bin
OBJS = lex.yy.o y.tab.o $(PROGRAM).o

all: $(PROGRAM)

$(PROGRAM): $(OBJS)
	$(CC) $(OBJS) -o $@

$(PROGRAM).o: $(PROGRAM).c $(PROGRAM).h

lex.yy.o: techtree.h

y.tab.o: y.tab.h

y.tab.h: grammar.y techtree.h
	$(YACC) -d $<

y.tab.c: grammar.y techtree.h

lex.h: lex.yy.c techtree.h
lex.yy.c: lexer.l y.tab.h techtree.h
	$(LEX) --header-file=lex.h lexer.l

.PHONY: clean install

clean:
	rm -f *.o $(PROGRAM)
	rm -f y.tab.c y.tab.h lex.yy.c lex.h

install: $(PROGRAM)
	$(INSTALL) $(PROGRAM) $(INSTALL_DIR)
